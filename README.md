INTRODUCTION
-----------
This module provides additional local task on node pages. 
The local task provides integration with BrightEdge and performs the SEO checks. 
Brightedge provides SEO recommendations.

REQUIREMENTS
------------
You need have an BrightEdge API account.

INSTALLATION
------------
To install this module, do the following:

Manual installation:
1. Download module from Drupal.org.
2. Place it alongside other contributed modules.
3. Install on Extend on page.

CONFIGURATION
-------------
Navigate to 'admin/config/brightedge_seo_check/brightedgeconfiguration' 
and provide BrightEdge API endpoint, Username and password.
