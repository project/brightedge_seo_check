<?php

namespace Drupal\brightedge_seo_check\Service;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7 as Stream;

/**
 * Class BrightEdgeClient.
 */
class BrightEdgeClient {

  protected $path;
  protected $config;

  /**
   * Constructs a new BrightEdgeClient object.
   */
  public function __construct() {
    $this->config = \Drupal::config('brightedge_seo_check.brightedgeconfiguration');
  }

  /**
   * Get BrightEdge Stats.
   */
  public function getStat($post_url) {
    // Set Guzzle HTTP Client.
    $client = new GuzzleClient();

    // Set Auth Header.
    $auth = base64_encode($this->config->get('api_username') . ':' . $this->config->get('api_password'));

    // Endpoint URL.
    $api_url = $this->config->get('api_endpoint');

    // Get html from page url.
    $html = file_get_contents($post_url);

    // Create temporary file and write html over there.
    $temp = tmpfile();
    fwrite($temp, $html);

    // Build PSR7 stream so to use for posting to BrightEdge.
    $stream = Stream\stream_for($temp);

    // Send request to BrightEdge.
    try {
      $res = $client->request(
        'POST', $api_url, [
          'headers' => [
            'Authorization' => 'Basic ' . $auth,
          ],
          'multipart' => [
          [
            'name'     => 'html_file',
            'Content-type' => 'multipart/form-data',
            'contents' => $stream,
          ],
          ],
        ]
      );

      $response = json_decode($res->getBody());

      // fclose, will unlink the tmpfile() as well.
      fclose($temp);

      return $response;
    }
    catch (\Exception $e) {
      // fclose, will unlink the tmpfile() as well.
      fclose($temp);

      // Log the exception to watchdog.
      watchdog_exception('type', $e);
      $message = $e->getMessage();

      \Drupal::messenger()->addMessage($message, 'error');

      return FALSE;
    }
  }

}
