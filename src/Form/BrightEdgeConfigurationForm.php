<?php

namespace Drupal\brightedge_seo_check\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BrightEdgeConfigurationForm.
 */
class BrightEdgeConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'brightedge_seo_check.brightedgeconfiguration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bright_edge_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('brightedge_seo_check.brightedgeconfiguration');
    $form['api_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Endpoint'),
      '#description' => $this->t('Enter BrightEdge API EndPoint'),
      '#maxlength' => 255,
      '#size' => 80,
      '#default_value' => $config->get('api_endpoint'),
    ];
    $form['api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service Account Username'),
      '#description' => $this->t('Enter Bright Edge API account username'),
      '#maxlength' => 255,
      '#size' => 80,
      '#default_value' => $config->get('api_username'),
    ];
    $form['api_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service Account Password'),
      '#description' => $this->t('Enter Bright Edge API password.'),
      '#maxlength' => 255,
      '#size' => 80,
      '#default_value' => $config->get('api_password'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('brightedge_seo_check.brightedgeconfiguration')
      ->set('api_endpoint', $form_state->getValue('api_endpoint'))
      ->set('api_username', $form_state->getValue('api_username'))
      ->set('api_password', $form_state->getValue('api_password'))
      ->save();
  }

}
