<?php

namespace Drupal\brightedge_seo_check\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;

/**
 * Class BrightEdgeController.
 */
class BrightEdgeController extends ControllerBase {

  /**
   * BrightEdgeStat.
   */
  public function brightEdgeStat(NodeInterface $node) {
    global $base_url;

    $post_url = $base_url . '/node/' . $node->id();

    $response = \Drupal::service('brightedge_seo_check.default')->getStat($post_url);

    // Seggregate Satisfied rules vs Non Satisfied rules.
    $satisfied_rules = [];
    $unsatisfied_rules = [];
    foreach ($response->recommendations as $key => $recommendation) {
      if ($recommendation->rule_satisfied == 0) {
        $unsatisfied_rules[$key]['title'] = $recommendation->rule_title;
        foreach ($recommendation->rule_details->actions as $action) {
          $unsatisfied_rules[$key]['action'][] = $action;
        }
        $unsatisfied_rules[$key]['importance'] = $recommendation->rule_details->importance[0];
      }
      else {
        $satisfied_rules[$key]['title'] = $recommendation->rule_title;
      }
    }

    return [
      '#theme' => 'brightedge_seo_check',
      '#score_details' => $response->recommendation_score_details,
      '#satisfied_rules' => $satisfied_rules,
      '#unsatisfied_rules' => $unsatisfied_rules,
    ];
  }

}
